package com.stefanstegic.CryptocurrencyRecommender.drools;

import com.stega.cryptorecommender.dto.CryptoCurrencyRecommendationDTO;
import com.stega.cryptorecommender.dto.nomics.MetadataDTO;
import com.stega.cryptorecommender.dto.nomics.TickerDTO;
import com.stega.cryptorecommender.filters.CryptocurrencyFilter;
import com.stega.cryptorecommender.model.Cryptocurrency;
import com.stega.cryptorecommender.services.PricePredictionService;
import com.stega.cryptorecommender.util.CryptoJsonUtil;
import com.stega.cryptorecommender.util.KieUtil;
import org.junit.Assert;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;

public class AssemblyAndFilterTests {

    @Test
    public void shouldAssembleCryptocurrency() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO metaBtc = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO tickerBtc = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");

        kieSession.insert(metaBtc);
        kieSession.insert(tickerBtc);

        kieSession.fireAllRules();

        Cryptocurrency result = (Cryptocurrency) kieSession.getObjects(new CryptocurrencyFilter()).toArray()[0];
        Assert.assertEquals(result.metaData.original_symbol, metaBtc.original_symbol);
        kieSession.dispose();
    }

    @Test
    public void shouldNotAssembleCryptocurrency() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO metaBtc = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        metaBtc.original_symbol = "OPA BATO";
        TickerDTO tickerBtc = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");

        kieSession.insert(metaBtc);
        kieSession.insert(tickerBtc);

        kieSession.fireAllRules();

        int count = kieSession.getObjects(new CryptocurrencyFilter()).toArray().length;
        assert(count == 0);

        kieSession.dispose();
    }

    @Test
    public void shouldAssembleIntoFinalDtos() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();
        ArrayList<CryptoCurrencyRecommendationDTO> result = new ArrayList<>();
        kieSession.setGlobal("recommendations", result);


        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        kieSession.insert(m1);
        kieSession.insert(t1);
        kieSession.fireAllRules();
        kieSession.getAgenda().getAgendaGroup("assemble-dtos").setFocus();
        kieSession.fireAllRules();

        assert(result.size() == 1);
        kieSession.dispose();
    }

}
