package com.stega.cryptorecommender.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import com.stega.cryptorecommender.dto.nomics.MetadataDTO;
import com.stega.cryptorecommender.dto.nomics.TickerDTO;
import com.stega.cryptorecommender.model.CryptoSocialMedia;
import com.stega.cryptorecommender.model.Cryptocurrency;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stega.cryptorecommender.dto.*;

@Service
public class DroolsService {
	
	@Autowired
	private NomicsCryptoService nomicsCryptoService;

	@Autowired
	private PricePredictionService pricePredictionService;
	
	@Autowired
	private KieContainer kieContainer;

	public ArrayList<CryptoCurrencyRecommendationDTO> findSuggestedCryptos(RecommendationParamsDTO params) {
		KieSession kSession = kieContainer.newKieSession();

		// Query parameters
		kSession.setGlobal("minMarketCapDollars", params.getMinMarketCapDollars());
		kSession.setGlobal("maxMarketCapDollars", params.getMaxMarketCapDollars());
		kSession.setGlobal("min24hVolume", params.getMin24hVolume());
		kSession.setGlobal("max24hVolume", params.getMax24hVolume());
		kSession.setGlobal("intervalDays", params.getDays());

		// Crypto facts
		ArrayList<MetadataDTO> metaCryptos = nomicsCryptoService.getMetaCryptos();
		CryptoSocialMedia socmed;
		for (MetadataDTO metaCrypto : metaCryptos) {
			kSession.insert(metaCrypto);
			for (String key : metaCrypto.socialMedia.keySet()) {
				socmed = new CryptoSocialMedia(metaCrypto.original_symbol, key, metaCrypto.socialMedia.get(key));
				kSession.insert(socmed);
			}
		}

		ArrayList<TickerDTO> tickerCryptos = nomicsCryptoService.getTickerCryptos();
		for (TickerDTO tickerCrypto : tickerCryptos) {
			kSession.insert(tickerCrypto);
		}

		ArrayList<CryptoCurrencyRecommendationDTO> result = new ArrayList<>();
		kSession.setGlobal("recommendations", result);
		kSession.setGlobal("pricePredictionService", pricePredictionService);

		kSession.fireAllRules();

		kSession.getAgenda().getAgendaGroup("assemble-dtos").setFocus();
		kSession.fireAllRules();

		HashSet<CryptoCurrencyRecommendationDTO> recSet = new HashSet<>();
		for (CryptoCurrencyRecommendationDTO rec : result) {
			recSet.add(rec);
		}

		result = new ArrayList<>(recSet);

		Collections.sort(result, (CryptoCurrencyRecommendationDTO c1, CryptoCurrencyRecommendationDTO c2) -> {
			return Double.compare(c2.getAveragePredictedProfit(), c1.getAveragePredictedProfit());
		});
		
		return result;
	}

}
