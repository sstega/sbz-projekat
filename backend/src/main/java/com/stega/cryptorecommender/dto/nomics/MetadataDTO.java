package com.stega.cryptorecommender.dto.nomics;

import lombok.Data;

import java.util.HashMap;

@Data
public class MetadataDTO {
    public String original_symbol;
    public String logo_url;
    public String name;
    public String website_url;
    public String whitepaper_url;
    public Integer markets_count;
    public Boolean used_for_pricing;
    public HashMap<String, String> socialMedia;

    public MetadataDTO() {
        socialMedia = new HashMap<>();
    }
}
