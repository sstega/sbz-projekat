package com.stega.cryptorecommender.dto;

import com.stega.cryptorecommender.model.Cryptocurrency;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CryptoCurrencyRecommendationDTO {
	private String symbol;
	private double currentPrice;
	private double predictedPrice7d, predictedPrice30d, predictedPrice365d;
	private double predictedProfit7d, predictedProfit30d, predictedProfit365d;
	private double averagePredictedPrice, averagePredictedProfit;
	private int score;

	public CryptoCurrencyRecommendationDTO(Cryptocurrency c, double predictedPrice7d, double predictedPrice30d, double predictedPrice365d, double predictedProfit7d, double predictedProfit30d, double predictedProfit365d) {
		symbol = c.ticker.symbol;
		score = c.score;
		currentPrice = c.ticker.price;
		this.predictedPrice7d = predictedPrice7d;
		this.predictedPrice30d = predictedPrice30d;
		this.predictedPrice365d = predictedPrice365d;
		this.predictedProfit7d = predictedProfit7d;
		this.predictedProfit30d = predictedProfit30d;
		this.predictedProfit365d = predictedProfit365d;
		this.averagePredictedProfit = (this.predictedProfit7d + this.predictedProfit30d + this.predictedProfit365d) / 3.0;
		this.averagePredictedPrice = (this.predictedPrice7d + this.predictedPrice30d + this.predictedPrice365d) / 3.0;
	}

	@Override
	public int hashCode() {
		return symbol.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		CryptoCurrencyRecommendationDTO rec = (CryptoCurrencyRecommendationDTO) obj;
		return symbol.equals(rec.symbol);
	}
}
