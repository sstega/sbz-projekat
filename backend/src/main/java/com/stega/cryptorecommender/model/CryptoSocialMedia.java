package com.stega.cryptorecommender.model;

public class CryptoSocialMedia {
    public String cryptoSymbol;
    public String name;
    public String value;

    public CryptoSocialMedia(String cryptoSymbol, String name, String value) {
        this.cryptoSymbol = cryptoSymbol;
        this.name = name;
        this.value = value;
    }
}
