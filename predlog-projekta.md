# SBNZ predlog projekta "Cryptocurrency Reccomender"

Student: Stefan Stegić
Indeks: SW 61/2017

## Motivacija

Tržište kriptovaluta, kao najspekulativniji i najnemirniji koncept u ekonomskoj istoriji, deluje primamljivo amaterskom investitoru. Međutim, mnogi od njih nisu dovoljno ekonomski obrazovani i/ili nisu spremni da ulože vreme u istraživanje novčića, što bi moglo da im poboljša šanse za profit.

## Problem

Korisnik želi da investira u tržište kriptovaluta. Zna minimalni profit koji želi da ostvari, vremenski interval do prodaje i kriterijume za izbor novčića. Potreban mu je ekspertski sistem koji će uzeti podatke o trenutnom i istorijskom stanju na tržištu kriptovaluta i ponuditi obećavajuće novčiće za kupovinu.

## Metodologija

### Ulaz

1. Trenutno i istorijsko stanje na tržištu kriptovaluta (eksterni API)
2. Vremenski interval izražen u danima
4. Min/max tržišna kapitalizacija novčića
5. Min/max promet u poslednjih 24h

### Izlaz

Spisak obećavajućih kriptovaluta u skladu sa kriterijumima korisnika.

## Pravila

Najpre se filtriraju kriptovalute po korisničkim kriterijumima. Izbacuju se iz razmatranja novčići čije tržišne kapitalizacije, prometi u poslednjih 24h i brojevi jedinica u cirkulaciji ne upadaju u zadate opsege.

Zatim, nad istorijskim podacima dostupnih kriptovaluta se radi linearna regresija i prikupljaju se predikcije cena u trenutku isteka zadatog vremenskog intervala.

Najzad, korisniku se predstavljaju novčići koji mogu u tom vremenskom intervalu da ostvare zadati profit.

### Detaljniji spisak pravila

Kriptovaluta NIJE aktivna
=> Izbacuje se iz razmatranja

Kriptovaluta se ne uklapa u korisnički zadate parametre
=> Izbacuje se iz razmatranja

Kriptovaluta ima barem jedan link ka nekoj društvenoj mreži
=> Prisutna je na društvenim mrežama

rule "A crypto that has at least one social media link is present on social media"
	when
		$c: Cryptocurrency( ) @Watch (!isScamCoin)
		Number($socmedCount: intValue > 0) from accumulate(
        	CryptoSocialMedia( $t: this, cryptoSymbol.equals($c.metaData.original_symbol), name != null, !name.trim().isEmpty(), value != null, !value.trim().isEmpty()),
        	count($t)
        )
	then
		System.out.println($c.ticker.symbol + " has social media: " + $socmedCount);
		modify($c) { hasSocialMedia = true; };
end

Kriptovaluta ima sajt, naučni rad ili je prisutna na društvenim mrežama
=> Nije scam coin, dobija bodove za:
1. Broj menjačnica na kojima je prisutna
2. Broj parova na menjačnicama čiji je deo
3. Gubi bodove jednake svom rangu po tržišnoj kapitalizaciji
4. Dobija bodove za broj meseci koliko postoji

Cena kriptovalute danas je manja nego što je bila pre 24h
=> Kriptovaluta je dnevno opadajuća, dobija bodove

Cena kriptovalute danas je manja nego što je bila pre nedelju dana
=> Kriptovaluta je nedeljno opadajuća, dobija bodove

Cena kriptovalute danas je veća nego što je bila pre mesec dana
=> Kriptovaluta je mesečno rastuća, dobija bodove

Cena kriptovalute danas je veća nego što je bila pre godinu dana
=> Kriptovaluta je godišnje rastuća, dobija bodove

Kriptovaluta je opadajuća u svakom slučaju
=> Izbacuje se iz razmatranja

Kriptovaluta je dnevno i nedeljno opadajuća, ali mesečno i godišnje rastuća
=> Nalazi se u kratkoročnom dipu i dobija bodove

Najzad, za sve preostale kriptovalute računa se price prediction i rangiraju se po bodovima u opadajućem redosledu.